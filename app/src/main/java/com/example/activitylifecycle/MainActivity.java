package com.example.activitylifecycle;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("ActivityLifeCycler","this is sub that want to merge in master");
        Log.d("add","add new one latest");
        Log.d("thidline","add third line master");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("ActivityLifeCycler","onStart");

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("ActivityLifeCycler","onResume");

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("ActivityLifeCycler","onPause");

    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("ActivityLifeCycler","onStop");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("ActivityLifeCycler","onDestroy");

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("ActivityLifeCycler","onRestart");

    }
}